package org.waterpicker.enchantmentapi;

import com.google.inject.Inject;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.util.JsonUtils;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.data.DataManager;
import org.spongepowered.api.data.DataRegistration;
import org.spongepowered.api.data.key.Key;
import org.spongepowered.api.event.game.GameRegistryEvent;
import org.spongepowered.api.event.game.state.GamePostInitializationEvent;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.plugin.PluginContainer;
import org.waterpicker.enchantmentapi.enchantment.*;
import org.waterpicker.enchantmentapi.enchantment.data.ImmutableStarEnchantmentData;
import org.waterpicker.enchantmentapi.enchantment.data.StarEnchantmentData;
import org.waterpicker.enchantmentapi.enchantment.data.StarEnchantmentDataBuilder;
import org.waterpicker.enchantmentapi.enchantment.data.StarEnchantmentDataManipulatorBuilder;

import java.io.IOException;

import static org.waterpicker.enchantmentapi.enchantment.data.Keys.ENCHANTMENTS;

@Plugin(
		id = "enchantmentapi",
		name = "EnchantmentAPI"
)
public class EnchantmentAPI {
	private static EnchantmentAPI instance;

	@Inject
	private Logger logger;

	@Inject
	private PluginContainer container;


	@Inject
	@DefaultConfig(sharedRoot = true)
	private ConfigurationLoader<CommentedConfigurationNode> configManager;

	public EnchantmentAPI() {
		instance = this;
	}

	public static EnchantmentAPI getInstance() {
		return instance;
	}

	@Listener
	public void onServerStart(GamePreInitializationEvent event) {
		CommandSpec.Builder spec = CommandSpec.builder();

		(new ClearEnchantsCommand()).register(spec);
		(new EnchantsCommand()).register(spec);
		(new EnchantCommand()).register(spec);

		Sponge.getCommandManager().register(this, spec.build(), "enchantmentapi");
	}

	@Listener
	public void onPostInit(GamePostInitializationEvent event) throws IOException {
		CommentedConfigurationNode node = configManager.load();

		StarEnchantmentHandler.values().forEach(enchantment -> {
			enchantment.createAndRegisterListeners();
			try {
				enchantment.loadConfig(node.getNode(enchantment.getName()));
			} catch (ObjectMappingException e) {
				e.printStackTrace();
			}
		});

		configManager.save(node);
	}

	@Listener
	public void onKeyRegister(GameRegistryEvent.Register<Key> event) {
		event.register(ENCHANTMENTS);
	}

	@Listener
	public void onDataRegistration(GameRegistryEvent.Register<DataRegistration<?, ?>> event) {
		DataManager dataManager = Sponge.getDataManager();
		dataManager.registerBuilder(StarEnchantment.class, new StarEnchantmentDataBuilder());

		DataRegistration.builder()
				.dataClass(StarEnchantmentData.class)
				.immutableClass(ImmutableStarEnchantmentData.class)
				.builder(new StarEnchantmentDataManipulatorBuilder())
				.manipulatorId("star_enchantments")
				.dataName("Star Enchantments")
				.buildAndRegister(container);
	}
}
