/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2015 socraticphoenix@gmail.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @author Socratic_Phoenix (socraticphoenix@gmail.com)
 */
package org.waterpicker.enchantmentapi.enchantment;

import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.ArmorEquipable;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.waterpicker.enchantmentapi.enchantment.data.StarEnchantmentData;

public class ClearEnchantsCommand implements CommandBase {
    @Override
    public CommandResult execute(CommandSource source, CommandContext context) {
        if(source instanceof ArmorEquipable) {
            ArmorEquipable equipable = (ArmorEquipable) source;
            if(equipable.getItemInHand(HandTypes.MAIN_HAND).isPresent()) {
                ItemStack stack = equipable.getItemInHand(HandTypes.MAIN_HAND).get();
                if(stack.get(StarEnchantmentData.class).isPresent()) {
                    stack = StarEnchantmentUtil.clearFormatting(stack);
                    stack.remove(StarEnchantmentData.class);
                }
                equipable.setItemInHand(HandTypes.MAIN_HAND, stack);
                source.sendMessage(Text.builder().color(TextColors.AQUA).append(Text.of("Removed all enchantments from your item!")).build());
            } else {
                source.sendMessage(Text.builder("You can't enchant air!").color(TextColors.RED).build());
            }
        } else {
            source.sendMessage(Text.builder("You aren't something that holds items...").color(TextColors.RED).build());
        }

        return CommandResult.success();
    }

    @Override
    public Text getDescription() {
        return Text.of("Clears enchantments.");
    }

    @Override
    public String getPermission() {
        return "enchantmentapi.command.enchant";
    }

    @Override
    public String[] getAliases() {
        return new String[] {
                "clear"
        };
    }

    @Override
    public CommandElement getArguments() {
        return GenericArguments.none();
    }
}
