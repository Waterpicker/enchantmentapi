/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2015 socraticphoenix@gmail.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @author Socratic_Phoenix (socraticphoenix@gmail.com)
 */
package org.waterpicker.enchantmentapi.enchantment;

import com.google.common.base.Preconditions;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataSerializable;
import org.spongepowered.api.data.MemoryDataContainer;
import org.spongepowered.api.data.Queries;
import org.waterpicker.enchantmentapi.enchantment.data.DataQueries;

public class StarEnchantment implements DataSerializable {
    private int level;
    private StarEnchantmentHandler handler;

    public StarEnchantment(int level, StarEnchantmentHandler handler) {
        Preconditions.checkArgument(level >= 0);
        this.level = level;
        this.handler = handler;
    }

    public StarEnchantment(int level, String handler)  {
        this(level, StarEnchantmentHandler.query(handler).orElseThrow(() -> new IllegalArgumentException("No handler registered for enchantment '".concat(handler).concat("'"))));
    }


    @Override
    public int getContentVersion() {
        return 1;
    }

    public StarEnchantment clone() {
        return new StarEnchantment(this.level, this.handler);
    }

    public int getLevel() {
        return this.level;
    }

    public StarEnchantmentHandler getHandler() {
        return this.handler;
    }

    @Override
    public DataContainer toContainer() {
        return new MemoryDataContainer()
                .set(Queries.CONTENT_VERSION, this.getContentVersion())
                .set(DataQueries.LEVEL, this.level)
                .set(DataQueries.ENCHANTMENT, this.handler.getName());
    }

}
