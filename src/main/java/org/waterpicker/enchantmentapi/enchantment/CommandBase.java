package org.waterpicker.enchantmentapi.enchantment;

import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;

public interface CommandBase extends CommandExecutor {
	public Text getDescription();

	public String getPermission();

	public String[] getAliases();

	public default void register(CommandSpec.Builder spec) {
		CommandSpec temp = CommandSpec.builder().description(getDescription()).permission(getPermission()).arguments(getArguments()).executor(this).build();
		spec.child(temp, getAliases());
	}

	CommandElement getArguments();
}
