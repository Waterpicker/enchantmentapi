/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2015 socraticphoenix@gmail.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @author Socratic_Phoenix (socraticphoenix@gmail.com)
 */
package org.waterpicker.enchantmentapi.enchantment.event;

import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.projectile.Projectile;
import org.spongepowered.api.event.Event;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.cause.entity.damage.source.EntityDamageSource;
import org.spongepowered.api.event.entity.DamageEntityEvent;
import org.spongepowered.api.event.entity.TargetEntityEvent;
import org.spongepowered.api.event.entity.projectile.TargetProjectileEvent;
import org.spongepowered.api.item.inventory.Carrier;
import org.waterpicker.enchantmentapi.enchantment.StarEnchantmentUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class EventProcessorRegistry {
    public static final String DEFAULT_PROCESSOR = "Event.default";

    private static final List<EventProcessor> processors = new ArrayList<>();

    static {
        registerDefaults();
    }

    public static void registerDefaults() { //NOTE: Register handlers in backwards inheritance order, so more specific events get their appropriate handlers used
        registerProcessor(new SimpleEventProcessor<>(TargetProjectileEvent.class, EventProcessor.TARGET_PROJECTILE_EVENT,
                event -> event.getTargetEntity().getShooter() instanceof Carrier,
                triple -> StarEnchantmentUtil.getByLocation((Carrier) triple.getLeft().getTargetEntity().getShooter(), triple.getRight(), triple.getMiddle())));

        registerProcessor(new SimpleEventProcessor<>(DamageEntityEvent.class, EventProcessor.DAMAGE_ENTITY_EVENT_ATTACKER,
                event -> event.getCause().containsType(EntityDamageSource.class) && event.getCause().first(EntityDamageSource.class).get().getSource() instanceof Carrier,
                triple -> StarEnchantmentUtil.getByLocation((Carrier) triple.getLeft().getCause().first(EntityDamageSource.class).get().getSource(), triple.getRight(), triple.getMiddle())));

        registerProcessor(new SimpleEventProcessor<>(DamageEntityEvent.class, EventProcessor.DAMAGE_ENTITY_EVENT_VICTIM,
                event -> event.getTargetEntity() instanceof Carrier,
                triple -> StarEnchantmentUtil.getByLocation((Carrier) triple.getLeft().getTargetEntity(), triple.getRight(), triple.getMiddle())));

        registerProcessor(new SimpleEventProcessor<>(DamageEntityEvent.class, EventProcessor.DAMAGE_ENTITY_EVENT_PROJECTILE,
                event ->  event.getCause().containsType(EntityDamageSource.class) && event.getCause().first(EntityDamageSource.class).get().getSource() instanceof Projectile && ((Projectile) event.getCause().first(EntityDamageSource.class).get().getSource()).getShooter() instanceof Carrier,
                triple ->  StarEnchantmentUtil.getByLocation((Carrier) ((Projectile) triple.getLeft().getCause().first(EntityDamageSource.class).get().getSource()).getShooter(), triple.getRight(), triple.getMiddle())));

        registerProcessor(new SimpleEventProcessor<>(TargetEntityEvent.class, EventProcessor.TARGET_ENTITY_EVENT,
                event -> event.getTargetEntity() instanceof Carrier,
                triple -> StarEnchantmentUtil.getByLocation((Carrier) triple.getLeft().getTargetEntity(), triple.getRight(), triple.getMiddle())));

        registerProcessor(new SimpleEventProcessor<>(InteractBlockEvent.class, EventProcessor.INTERACT_BLOCK_EVENT,
                event -> event.getCause().first(Entity.class).isPresent() && Carrier.class.isAssignableFrom(event.getCause().first(Entity.class).get().getClass()),
                triple -> StarEnchantmentUtil.getByLocation((Carrier) triple.getLeft().getCause().first(Entity.class).get(), triple.getRight(), triple.getMiddle())));

        registerProcessor(new SimpleEventProcessor<>(ChangeBlockEvent.class, EventProcessor.CHANGE_BLOCK_EVENT,
                event -> event.getCause().first(Entity.class).isPresent() && Carrier.class.isAssignableFrom(event.getCause().first(Entity.class).get().getClass()),
                triple -> StarEnchantmentUtil.getByLocation((Carrier) triple.getLeft().getCause().first(Entity.class).get(), triple.getRight(), triple.getMiddle())));
    }

    public static <T extends Event> Optional<EventProcessor<T>> getAppropriateProcessor(Class<T> event, String id) {
        return id.equals(EventProcessorRegistry.DEFAULT_PROCESSOR) ? EventProcessorRegistry.getAppropriateProcessor(event) : Optional.ofNullable(EventProcessorRegistry.processors.stream().filter(processor -> processor.accepts(event) && processor.id().equals(id)).findFirst().orElse(null));
    }

    public static <T extends Event> Optional<EventProcessor<T>> getAppropriateProcessor(Class<T> event) {
        return Optional.ofNullable(EventProcessorRegistry.processors.stream().filter(processor -> processor.accepts(event)).findFirst().orElse(null));
    }

    public static void registerProcessor(EventProcessor processor) {
        EventProcessorRegistry.processors.add(processor);
    }

}
