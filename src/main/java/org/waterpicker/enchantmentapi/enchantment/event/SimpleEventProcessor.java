/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2015 socraticphoenix@gmail.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @author Socratic_Phoenix (socraticphoenix@gmail.com)
 */
package org.waterpicker.enchantmentapi.enchantment.event;

import org.apache.commons.lang3.tuple.Triple;
import org.spongepowered.api.event.Event;
import org.spongepowered.api.item.inventory.ItemStack;
import org.waterpicker.enchantmentapi.enchantment.EnchantmentLocation;
import org.waterpicker.enchantmentapi.enchantment.StarEnchantment;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class SimpleEventProcessor<T extends Event> implements EventProcessor<T> {
    private Class<T> event;
    private String id;
    private Predicate<T> acceptor;
    private Function<Triple<T, EnchantmentLocation, String>, List<Triple<ItemStack, StarEnchantment, EnchantmentLocation>>> processor;

    public SimpleEventProcessor(Class<T> event, String id, Predicate<T> acceptor, Function<Triple<T, EnchantmentLocation, String>, List<Triple<ItemStack, StarEnchantment, EnchantmentLocation>>> processor) {
        this.event = event;
        this.id = id;
        this.acceptor = acceptor;
        this.processor = processor;
    }

    @Override
    public boolean accepts(Class<? extends Event> clazz) {
        return this.event.isAssignableFrom(clazz);
    }

    @Override
    public boolean accepts(T event) {
        return this.acceptor.test(event);
    }

    @Override
    public List<Triple<ItemStack, StarEnchantment, EnchantmentLocation>> process(T event, EnchantmentLocation location, String target) {
        return this.processor.apply(Triple.of(event, location, target));
    }

    @Override
    public String id() {
        return this.id;
    }

}
