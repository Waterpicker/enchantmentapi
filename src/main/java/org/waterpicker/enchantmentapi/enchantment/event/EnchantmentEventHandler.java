/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2015 socraticphoenix@gmail.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @author Socratic_Phoenix (socraticphoenix@gmail.com)
 */
package org.waterpicker.enchantmentapi.enchantment.event;

import org.apache.commons.lang3.tuple.Triple;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.event.Cancellable;
import org.spongepowered.api.event.Event;
import org.spongepowered.api.event.EventListener;
import org.spongepowered.api.item.inventory.ItemStack;
import org.waterpicker.enchantmentapi.EnchantmentAPI;
import org.waterpicker.enchantmentapi.enchantment.EnchantmentLocation;
import org.waterpicker.enchantmentapi.enchantment.StarEnchantment;
import org.waterpicker.enchantmentapi.enchantment.StarEnchantmentHandler;

import java.util.List;

public class EnchantmentEventHandler<T extends Event> implements EventListener<T> {
    private EventProcessor<T> processor;
    private boolean ignoreCancelled;
    private EnchantmentLocation location;
    private StarEnchantmentHandler enchantment;
    private WrappedHandler handler;
    private Class<T> event;

    public EnchantmentEventHandler(StarEnchantmentHandler enchantment, WrappedHandler handler, Class<T> event, EnchantmentHandler annotation) {
        this.ignoreCancelled = annotation.ignoreCancelled() && Cancellable.class.isAssignableFrom(event);
        this.location = annotation.location();
        this.processor = EventProcessorRegistry.getAppropriateProcessor(event, annotation.processor()).orElseThrow(() -> new IllegalStateException("No processor registered under name '".concat(annotation.processor()).concat("' for event '").concat(event.getName()).concat("'")));
        this.enchantment = enchantment;
        this.handler = handler;
        this.event = event;
    }

    @Override
    public void handle(T event) throws Exception {
        if(this.ignoreCancelled && event instanceof Cancellable && ((Cancellable) event).isCancelled()) {
            return;
        }

        if (this.processor.accepts(event)) {
            List<Triple<ItemStack, StarEnchantment, EnchantmentLocation>> triples = this.processor.process(event, this.location, this.enchantment.getName());
            triples.forEach(triple -> {
                this.handler.invoke(this.enchantment, event, triple.getLeft(), triple.getMiddle(), triple.getRight());
            });
        }
    }

    public void register() {
        Sponge.getEventManager().registerListener(EnchantmentAPI.getInstance(), this.event, this);
    }

}
