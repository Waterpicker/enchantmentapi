/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2015 socraticphoenix@gmail.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @author Socratic_Phoenix (socraticphoenix@gmail.com)
 */
package org.waterpicker.enchantmentapi.enchantment;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.tuple.Triple;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.item.LoreData;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.ArmorEquipable;
import org.spongepowered.api.item.inventory.Carrier;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.inventory.entity.Hotbar;
import org.spongepowered.api.item.inventory.property.SlotIndex;
import org.spongepowered.api.item.inventory.type.GridInventory;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColor;
import org.spongepowered.api.text.format.TextColors;
import org.waterpicker.enchantmentapi.enchantment.data.StarEnchantmentData;
import org.waterpicker.enchantmentapi.util.RomanNumber;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class StarEnchantmentUtil {
    public static final String ENCHANTMENTS_HEADER = "Custom Enchantments:";

    public static ItemStack clearFormatting(ItemStack stack) {
        ItemStack copy = stack.copy();
        if(copy.get(LoreData.class).isPresent()) {
            LoreData loreData = copy.get(LoreData.class).get();
            List<Text> texts = new ArrayList<>();
            for (Text text : loreData.get(Keys.ITEM_LORE).get()) {
                if (text.toPlain().equals(StarEnchantmentUtil.ENCHANTMENTS_HEADER)) {
                    if(!texts.isEmpty()) {
                        texts.remove(texts.size() - 1);
                    }
                    break;
                } else {
                    texts.add(text);
                }
            }
            copy.offer(Keys.ITEM_LORE, texts);
        }
        return copy;
    }

    public static ItemStack formatEnchantments(ItemStack stack) {
        ItemStack copy = stack.copy();
        if(copy.get(StarEnchantmentData.class).isPresent()) {
            StarEnchantmentData data = copy.get(StarEnchantmentData.class).get();
            List<Text> lores = new ArrayList<>();
            data.enchantments().forEach(enchantment -> {
                lores.add(Text.builder().append(Text.of(TextColors.GRAY, enchantment.getHandler().humanReadableName())).append(Text.builder(" ".concat(RomanNumber.toRoman(enchantment.getLevel()))).color(TextColors.GRAY).build()).build());
            });

            copy.offer(Keys.ITEM_LORE, lores);
            return copy;
        } else {
            return copy;
        }
    }

    public static List<StarEnchantment> getEnchantments(ItemStack stack) {
        if (stack.get(StarEnchantmentData.class).isPresent()) {
            return stack.get(StarEnchantmentData.class).get().enchantments().get();
        } else {
            return new ArrayList<>();
        }
    }

    public static List<StarEnchantment> getEnchantments(ItemStack stack, String target) {
        if (stack.get(StarEnchantmentData.class).isPresent()) {
            return stack.get(StarEnchantmentData.class).get().enchantments().get().stream().filter(enchantment -> enchantment.getHandler().getName().equals(target)).collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

    public static Optional<StarEnchantment> getEnchantment(ItemStack stack, String target) {
        if (stack.get(StarEnchantmentData.class).isPresent()) {
            return stack.get(StarEnchantmentData.class).get().enchantments().get().stream().filter(enchantment -> enchantment.getHandler().getName().equals(target)).min((a, b) -> b.getLevel() - a.getLevel());
        } else {
            return Optional.empty();
        }
    }

    public static List<Triple<ItemStack, StarEnchantment, EnchantmentLocation>> getAllByLocation(Carrier carrier, EnchantmentLocation location) {
        List<Triple<ItemStack, StarEnchantment, EnchantmentLocation>> triples = new ArrayList<>();
        StarEnchantmentHandler.values().forEach(constant -> triples.addAll(StarEnchantmentUtil.getByLocation(carrier, constant.getName(), location)));
        return triples;
    }

    public static List<Triple<ItemStack, StarEnchantment, EnchantmentLocation>> getByLocation(Carrier carrier, String target, EnchantmentLocation location) {
        List<Triple<ItemStack, StarEnchantment, EnchantmentLocation>> triples = new ArrayList<>();
        switch (location) {
            case ANY:
                //Armor
                triples.addAll(StarEnchantmentUtil.getBoots(carrier, target));
                triples.addAll(StarEnchantmentUtil.getLeggings(carrier, target));
                triples.addAll(StarEnchantmentUtil.getChestplate(carrier, target));
                triples.addAll(StarEnchantmentUtil.getHelmet(carrier, target));

                //Hotbar (exclude in hand, because Mobs don't have a hotbar)
                triples.addAll(StarEnchantmentUtil.getHotbar(carrier, target, true));

                //In Hand
                triples.addAll(StarEnchantmentUtil.getHand(carrier, target));

                //Main inventory
                triples.addAll(StarEnchantmentUtil.getMain(carrier, target));
                break;
            case HAND:
                triples.addAll(StarEnchantmentUtil.getHand(carrier, target));
                break;
            case MAIN:
                triples.addAll(StarEnchantmentUtil.getMain(carrier, target));
                break;
            case HELMET:
                triples.addAll(StarEnchantmentUtil.getHelmet(carrier, target));
                break;
            case CHESTPLATE:
                triples.addAll(StarEnchantmentUtil.getChestplate(carrier, target));
                break;
            case LEGGINGS:
                triples.addAll(StarEnchantmentUtil.getLeggings(carrier, target));
                break;
            case BOOTS:
                triples.addAll(StarEnchantmentUtil.getBoots(carrier, target));
                break;
            case ARMOR:
                triples.addAll(StarEnchantmentUtil.getBoots(carrier, target));
                triples.addAll(StarEnchantmentUtil.getLeggings(carrier, target));
                triples.addAll(StarEnchantmentUtil.getChestplate(carrier, target));
                triples.addAll(StarEnchantmentUtil.getHelmet(carrier, target));
                break;
            case HOTBAR:
                triples.addAll(StarEnchantmentUtil.getHotbar(carrier, target, false));
                break;
        }

        return triples;
    }

    public static List<Triple<ItemStack, StarEnchantment, EnchantmentLocation>> getHand(Carrier carrier, String target){
        if (carrier instanceof ArmorEquipable) {
            Optional<ItemStack> stackOptional = ((ArmorEquipable) carrier).getItemInHand(HandTypes.MAIN_HAND);
            if (stackOptional.isPresent() && stackOptional.get().get(StarEnchantmentData.class).isPresent()) {
                Optional<StarEnchantment> enchantment = StarEnchantmentUtil.getEnchantment(stackOptional.get(), target);
                if (enchantment.isPresent()) {
                    return Lists.newArrayList(Triple.of(stackOptional.get(), enchantment.get(), EnchantmentLocation.HAND));
                }
            }
        }

        return Lists.newArrayList();
    }

    public static List<Triple<ItemStack, StarEnchantment, EnchantmentLocation>> getMain(Carrier carrier, String target) {
        List<Triple<ItemStack, StarEnchantment, EnchantmentLocation>> triples = new ArrayList<>();

        Inventory sub = carrier.getInventory().query(GridInventory.class);
        if(sub instanceof GridInventory) {
            GridInventory grid = (GridInventory) sub;
            grid.slots().forEach(slot -> {
                Optional<ItemStack> stackOptional = slot.peek();
                if (stackOptional.isPresent() && stackOptional.get().get(StarEnchantmentData.class).isPresent()) {
                    Optional<StarEnchantment> enchantment = StarEnchantmentUtil.getEnchantment(stackOptional.get(), target);
                    if (enchantment.isPresent()) {
                        triples.add(Triple.of(stackOptional.get(), enchantment.get(), EnchantmentLocation.MAIN));
                    }
                }
            });
        }

        return triples;
    }

    public static List<Triple<ItemStack, StarEnchantment, EnchantmentLocation>> getHotbar(Carrier carrier, String target, boolean excludeHolding) {
        List<Triple<ItemStack, StarEnchantment, EnchantmentLocation>> triples = new ArrayList<>();

        Inventory sub = carrier.getInventory().query(Hotbar.class);
        if (sub instanceof Hotbar) {
            Hotbar hotbar = (Hotbar) sub;
            int holding = hotbar.getSelectedSlotIndex();
            for (int i = 0; i < 9; i++) {
                if (!excludeHolding || i != holding) {
                    Optional<Slot> slot = hotbar.getSlot(new SlotIndex(i));
                    if (slot.isPresent()) {
                        Optional<ItemStack> stackOptional = slot.get().peek();
                        if (stackOptional.isPresent() && stackOptional.get().get(StarEnchantmentData.class).isPresent()) {
                            Optional<StarEnchantment> enchantment = StarEnchantmentUtil.getEnchantment(stackOptional.get(), target);
                            if (enchantment.isPresent()) {
                                triples.add(Triple.of(stackOptional.get(), enchantment.get(), i == holding ? EnchantmentLocation.HAND : EnchantmentLocation.HOTBAR));
                            }
                        }
                    }
                }
            }
        }

        return triples;
    }

    public static List<Triple<ItemStack, StarEnchantment, EnchantmentLocation>> getHelmet(Carrier carrier, String target) {
        if (carrier instanceof ArmorEquipable) {
            Optional<ItemStack> stackOptional = ((ArmorEquipable) carrier).getHelmet();
            if (stackOptional.isPresent() && stackOptional.get().get(StarEnchantmentData.class).isPresent()) {
                Optional<StarEnchantment> enchantment = StarEnchantmentUtil.getEnchantment(stackOptional.get(), target);
                if (enchantment.isPresent()) {
                    return Lists.newArrayList(Triple.of(stackOptional.get(), enchantment.get(), EnchantmentLocation.HELMET));
                }
            }
        }

        return Lists.newArrayList();
    }

    public static List<Triple<ItemStack, StarEnchantment, EnchantmentLocation>> getChestplate(Carrier carrier, String target) {
        if (carrier instanceof ArmorEquipable) {
            Optional<ItemStack> stackOptional = ((ArmorEquipable) carrier).getChestplate();
            if (stackOptional.isPresent() && stackOptional.get().get(StarEnchantmentData.class).isPresent()) {
                Optional<StarEnchantment> enchantment = StarEnchantmentUtil.getEnchantment(stackOptional.get(), target);
                if (enchantment.isPresent()) {
                    return Lists.newArrayList(Triple.of(stackOptional.get(), enchantment.get(), EnchantmentLocation.CHESTPLATE));
                }
            }
        }

        return Lists.newArrayList();
    }

    public static List<Triple<ItemStack, StarEnchantment, EnchantmentLocation>> getBoots(Carrier carrier, String target) {
        if (carrier instanceof ArmorEquipable) {
            Optional<ItemStack> stackOptional = ((ArmorEquipable) carrier).getBoots();
            if (stackOptional.isPresent() && stackOptional.get().get(StarEnchantmentData.class).isPresent()) {
                Optional<StarEnchantment> enchantment = StarEnchantmentUtil.getEnchantment(stackOptional.get(), target);
                if (enchantment.isPresent()) {
                    return Lists.newArrayList(Triple.of(stackOptional.get(), enchantment.get(), EnchantmentLocation.BOOTS));
                }
            }
        }

        return Lists.newArrayList();
    }

    public static List<Triple<ItemStack, StarEnchantment, EnchantmentLocation>> getLeggings(Carrier carrier, String target) {
        if (carrier instanceof ArmorEquipable) {
            Optional<ItemStack> stackOptional = ((ArmorEquipable) carrier).getLeggings();
            if (stackOptional.isPresent() && stackOptional.get().get(StarEnchantmentData.class).isPresent()) {
                Optional<StarEnchantment> enchantment = StarEnchantmentUtil.getEnchantment(stackOptional.get(), target);
                if (enchantment.isPresent()) {
                    return Lists.newArrayList(Triple.of(stackOptional.get(), enchantment.get(), EnchantmentLocation.LEGGINGS));
                }
            }
        }

        return Lists.newArrayList();
    }

}
