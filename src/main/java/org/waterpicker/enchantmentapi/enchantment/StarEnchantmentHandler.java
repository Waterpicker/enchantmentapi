/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2015 socraticphoenix@gmail.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @author Socratic_Phoenix (socraticphoenix@gmail.com)
 */
package org.waterpicker.enchantmentapi.enchantment;

import com.google.common.reflect.TypeToken;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.event.Event;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.plugin.PluginContainer;
import org.waterpicker.enchantmentapi.EnchantmentAPI;
import org.waterpicker.enchantmentapi.enchantment.event.EnchantmentEventHandler;
import org.waterpicker.enchantmentapi.enchantment.event.EnchantmentHandler;
import org.waterpicker.enchantmentapi.enchantment.event.WrappedHandler;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

public abstract class StarEnchantmentHandler {
    private static Map<String, StarEnchantmentHandler> handlers = new HashMap<>();

    private boolean generated = false;
    private String name;
    private List<String> whitelist;

    public static void init(){}; //Serves the purpose of fully loading the class, and as such its  public static final fields

    public StarEnchantmentHandler(String name) {
        this.name = name;
        handlers.put(name, this);
        System.out.println(handlers);
    }

    public static Optional<StarEnchantmentHandler> query(String handler) {
        return Optional.ofNullable(handlers.get(handler));
    }

    public static Collection<StarEnchantmentHandler> values() {
        return handlers.values();
    }

    public static Set<String> keys() {
        return handlers.keySet();
    }

    public abstract String humanReadableName();

    public static String generateName(String pluginid, String name) {
        return pluginid.concat(":").concat(name);
    }

    public static String generateName(PluginContainer container, String name) {
        return StarEnchantmentHandler.generateName(container.getId(), name);
    }

    public static String generateName(Object plugin, String name) {
        return StarEnchantmentHandler.generateName(Sponge.getPluginManager().fromInstance(plugin).orElseThrow(() -> new IllegalArgumentException("Could not locate plugin instance for object of type \"".concat(plugin.getClass().getName()).concat("\""))), name);
    }

    public void createAndRegisterListeners() {
        if(!this.generated) {
            this.generateListeners();
            this.registerNativeListeners();
            this.generated = true;
        }
    }

    private void registerNativeListeners() {
        if(Stream.of(this.getClass().getDeclaredMethods()).anyMatch(method -> method.isAnnotationPresent(Listener.class))) {
            Sponge.getEventManager().registerListeners(EnchantmentAPI.getInstance(), this);
        }
    }

    private void generateListeners() {
        for(Method method : this.getClass().getDeclaredMethods()) {
            if(!Modifier.isStatic(method.getModifiers()) && method.isAnnotationPresent(EnchantmentHandler.class)) {
                Parameter[] parameters = method.getParameters();
                if(parameters.length != 4 ) {
                    throw new IllegalArgumentException(new StringBuilder()
                            .append("Enchantment handler \"")
                            .append(this.getClass().getName())
                            .append("#")
                            .append(method.getName())
                            .append("\" does not have parameters of the form (Event event, ItemStack stack, StarEnchantment enchantment, EnchantmentLocation location)")
                            .toString());
                }

                if(Event.class.isAssignableFrom(parameters[0].getType()) && ItemStack.class.isAssignableFrom(parameters[1].getType()) && StarEnchantment.class.isAssignableFrom(parameters[2].getType()) && EnchantmentLocation.class.isAssignableFrom(parameters[3].getType())) {
                    WrappedHandler handler = new WrappedHandler(method);
                    Class event = parameters[0].getType();
                    EnchantmentEventHandler enchantmentEventHandler = new EnchantmentEventHandler(this, handler, event, method.getAnnotation(EnchantmentHandler.class));
                    enchantmentEventHandler.register();
                } else {
                    throw new IllegalArgumentException(new StringBuilder()
                            .append("Enchantment handler \"")
                            .append(this.getName())
                            .append("#")
                            .append(method.getName())
                            .append("\" does not have parameters of the form (Event event, ItemStack stack, StarEnchantment enchantment, EnchantmentLocation location)")
                            .toString());
                }

            }
        }
    }

    public boolean isAllowedOn(ItemStack type) {
        return whitelist.isEmpty() || whitelist.contains(type.getType().getId());
    }

    public String getName() {
        return name;
    }

    public List<String> getDefaultWhiteList() {
        return new ArrayList<>();
    }


    public void loadConfig(CommentedConfigurationNode node) throws ObjectMappingException {
        node.getNode("whitelist").setValue(whitelist = node.getNode("whitelist").getList(TypeToken.of(String.class), this::getDefaultWhiteList));
    }
}
