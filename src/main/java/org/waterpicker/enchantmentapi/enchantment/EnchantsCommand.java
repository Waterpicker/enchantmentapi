/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2015 socraticphoenix@gmail.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @author Socratic_Phoenix (socraticphoenix@gmail.com)
 */
package org.waterpicker.enchantmentapi.enchantment;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.service.pagination.PaginationList;
import org.spongepowered.api.service.pagination.PaginationService;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.ArrayList;
import java.util.List;

public class EnchantsCommand implements CommandBase {

    @Override
    public CommandResult execute(CommandSource source, CommandContext context) {
        if(!StarEnchantmentHandler.values().isEmpty()) {
            PaginationList.Builder builder = Sponge.getServiceManager().provideUnchecked(PaginationService.class).builder();
            List<Text> texts = new ArrayList<>();
            builder.padding(Text.of("="));
            builder.title(Text.builder("StarAPI Enchantments").color(TextColors.BLUE).build());
            StarEnchantmentHandler.values().forEach(constant -> {
                texts.add(Text.builder("- ").append(Text.of(constant.humanReadableName())).append(Text.of(" (id: ".concat(constant.getName()).concat(")"))).color(TextColors.AQUA).build());
            });
            builder.contents(texts);
            builder.sendTo(source);
        } else {
            source.sendMessage(Text.builder("No enchantments are registered!").color(TextColors.RED).build());
        }
        return CommandResult.success();
    }

    @Override
    public Text getDescription() {
        return Text.of("Lists all custom StarAPI enchantments currently registered.");
    }

    @Override
    public String getPermission() {
        return "enchantmentapi.command.enchants";
    }

    @Override
    public String[] getAliases() {
        return new String[] {
                "list"
        };
    }

    @Override
    public CommandElement getArguments() {
        return GenericArguments.none();
    }

}
