/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2015 socraticphoenix@gmail.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @author Socratic_Phoenix (socraticphoenix@gmail.com)
 */
package org.waterpicker.enchantmentapi.enchantment;

import com.google.common.collect.Lists;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.ArmorEquipable;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.waterpicker.enchantmentapi.enchantment.data.StarEnchantmentData;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class EnchantCommand implements CommandBase {

    @Override
    public CommandResult execute(CommandSource source, CommandContext arguments) {
        Optional<Player> player = arguments.getOne(Text.of("player"));
        Optional<StarEnchantmentHandler> enchantment = arguments.getOne(Text.of("enchantment"));
        Optional<Integer> level = arguments.getOne(Text.of("level"));

        if(player.isPresent()) {
            ArmorEquipable equipable = player.get();
            Optional<ItemStack> stack = equipable.getItemInHand(HandTypes.MAIN_HAND).filter(s -> !s.isEmpty());

            if(stack.isPresent() && enchantment.isPresent() && level.isPresent()) {
                StarEnchantment starEnchantment = new StarEnchantment(level.get(), enchantment.get());
                ItemStack s = equipable.getItemInHand(HandTypes.MAIN_HAND).get();

                if(!starEnchantment.getHandler().isAllowedOn(s)) {
                    source.sendMessage(Text.builder("\"".concat(enchantment.get().getName()).concat("\"") + " can not be added to " + s.getType().getName()).color(TextColors.RED).build());
                    return CommandResult.empty();
                }

                equipable.setItemInHand(HandTypes.MAIN_HAND, this.addEnchant(s, starEnchantment));
                source.sendMessage(Text.builder("Successfully enchanted your item with \"".concat(enchantment.get().getName()).concat("\"")).color(TextColors.AQUA).build());
                return CommandResult.success();
            } else {
                source.sendMessage(Text.builder("You can't enchant air!").color(TextColors.RED).build());
                return CommandResult.empty();
            }
        } else {
            source.sendMessage(Text.builder("You aren't something that holds items...").color(TextColors.RED).build());
            return CommandResult.empty();
        }
    }

    @Override
    public Text getDescription() {
        return Text.of("Enchants your item.");
    }

    private ItemStack addEnchant(ItemStack stack, StarEnchantment enchantment) {
        ItemStack copy = stack.copy();
        if (copy.get(StarEnchantmentData.class).isPresent()) {
            StarEnchantmentData data = copy.get(StarEnchantmentData.class).get();
            List<StarEnchantment> enchantments = new ArrayList<>();
            data.getListValue().get().stream().filter(enchantment1 -> !enchantment.getHandler().getName().equals(enchantment1.getHandler().getName())).forEach(enchantments::add);
            enchantments.add(enchantment);
            copy.offer(new StarEnchantmentData(enchantments));
            copy = StarEnchantmentUtil.clearFormatting(copy);
            return StarEnchantmentUtil.formatEnchantments(copy);
        } else {
            StarEnchantmentData data = new StarEnchantmentData(Lists.newArrayList(enchantment));
            copy.offer(data);
            return StarEnchantmentUtil.formatEnchantments(copy);
        }

    }

    @Override
    public CommandElement getArguments() {
        return GenericArguments.seq(
                GenericArguments.player(Text.of("player")),
                GenericArguments.choices(Text.of("enchantment"), StarEnchantmentHandler::keys, s -> StarEnchantmentHandler.query(s).orElse(null)),
                GenericArguments.integer(Text.of("level")));
    }

    @Override
    public String getPermission() {
        return "enchantmentapi.command.enchant";
    }

    @Override
    public String[] getAliases() {
        return new String[] {
                "enchant"
        };
    }
}
