package org.waterpicker.enchantmentapi.enchantment.data;

import com.google.common.reflect.TypeToken;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.key.Key;
import org.spongepowered.api.data.value.mutable.ListValue;
import org.waterpicker.enchantmentapi.enchantment.StarEnchantment;

import javax.annotation.Nullable;

public class Keys {
	public static final Key<ListValue<StarEnchantment>> ENCHANTMENTS = Key.builder().id("star_enchantments").name("Star Enchantments").query(DataQuery.of("Star Enchantments")).type(new TypeToken<ListValue<StarEnchantment>>() {}).build(); //KeyFactory.makeListKey(StarEnchantment.class, DataQuery.of("Star_Enchantments"));
}
