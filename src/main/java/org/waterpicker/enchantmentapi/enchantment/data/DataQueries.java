package org.waterpicker.enchantmentapi.enchantment.data;

import org.spongepowered.api.data.DataQuery;

public class DataQueries {
	public static final DataQuery ENCHANTMENT = DataQuery.of("Star_Enchantment");
	public static final DataQuery LEVEL = DataQuery.of("Star_Level");
}
