/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2015 socraticphoenix@gmail.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @author Socratic_Phoenix (socraticphoenix@gmail.com)
 */
package org.waterpicker.enchantmentapi.enchantment.data;

import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.Queries;
import org.spongepowered.api.data.manipulator.mutable.common.AbstractListData;
import org.spongepowered.api.data.merge.MergeFunction;
import org.spongepowered.api.data.value.mutable.ListValue;
import org.waterpicker.enchantmentapi.enchantment.StarEnchantment;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class StarEnchantmentData extends AbstractListData<StarEnchantment, StarEnchantmentData, ImmutableStarEnchantmentData> {

    public StarEnchantmentData() {
        this(new ArrayList<>());
    }

    public StarEnchantmentData(List<StarEnchantment> value) {
        super(value, Keys.ENCHANTMENTS);
    }

    public ListValue<StarEnchantment> enchantments() {
        return getValueGetter();
    }

    @Override
    public DataContainer toContainer() {
        return super.toContainer()
                .set(Queries.CONTENT_VERSION, this.getContentVersion())
                .set(Keys.ENCHANTMENTS.getQuery(), this.getValue());
    }

    @Override
    public Optional<StarEnchantmentData> fill(DataHolder dataHolder, MergeFunction overlap) {
        if(dataHolder.toContainer().contains(Keys.ENCHANTMENTS)) {
            List<StarEnchantment> enchantments = dataHolder.toContainer().getSerializableList(Keys.ENCHANTMENTS.getQuery(), StarEnchantment.class).get();
            ListValue<StarEnchantment> existing = this.enchantments();
            existing.addAll(enchantments);
            this.setValue(existing.get());
            return Optional.of(this);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<StarEnchantmentData> from(DataContainer container) {
        if(container.contains(Keys.ENCHANTMENTS)) {
            List<StarEnchantment> enchantments = container.getSerializableList(Keys.ENCHANTMENTS.getQuery(), StarEnchantment.class).get();
            this.setValue(enchantments);
            return Optional.of(this);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public StarEnchantmentData copy() {
        return new StarEnchantmentData(this.clone(this.getValue()));
    }

    @Override
    public ImmutableStarEnchantmentData asImmutable() {
        return new ImmutableStarEnchantmentData(this.clone(this.getValue()));
    }

    private List<StarEnchantment> clone(List<StarEnchantment> enchantments) {
        List<StarEnchantment> enchantmentList = new ArrayList<>();
        enchantments.forEach(enchantment -> enchantmentList.add(enchantment.clone()));
        return enchantmentList;
    }

    @Override
    public int getContentVersion() {
        return 1;
    }
}
